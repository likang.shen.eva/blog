# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

1.1 Installing Rails

```
$ ruby -v
  ruby 2.3.0p0
```

Verify that it is correctly installed and in your PATH:

```
$ sqlite3 --version
```

To install Rails, use the gem install command provided by RubyGems:

```
$ gem install rails
```

To verify that you have everything installed correctly, you should be able to run the following:

```
$ rails --version
```
If it says something like "Rails 5.0.0", you are ready to continue.

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

```
$ bin/rails server
```

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
