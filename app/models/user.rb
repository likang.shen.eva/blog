class User < ApplicationRecord
  validates :id, presence: true,
                    length: { maximum: 3 }
end
