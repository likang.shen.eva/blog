class UsersController < ApplicationController
	def index
    @users = User.all
    end
    def show
    @user = User.find(params[:id])
    end
	def new
	@user = User.new
	end
	def edit
    @user = User.find(params[:id])
    end
    def create
  	@user = User.new(user_params)
 	if @user.save
    	redirect_to @user, flash: {success: "New account registration is successful, please login"}
  	else
    	render 'new'
  	end
  	end

    end
	def edit
  	@user = User.find(params[:id])
	end
	
	def user_params
    params.require(:user).permit(:id, :name)
  	end
  	
  	def destroy
    @user = User.find_by_id(params[:id])
    @user.destroy
    redirect_to users_path(new: false), flash: {success: "User has deleted"}
    end
    
    private
    def user_params
    params.require(:user).permit(:id, :name)
    end
    def logged_in
    unless logged_in?
      redirect_to root_url, flash: {danger: 'Please log in'}
    end
end
